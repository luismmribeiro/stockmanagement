package org.luismmribeiro.tutorials.stockmanagement.domain;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * The product Entity
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Entity
@NamedQuery(name=Product.GET_ALL_PRODUCTS_QUERY_NAME, query="SELECT p FROM Product p")
public class Product extends GenericEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String GET_ALL_PRODUCTS_QUERY_NAME = "Product.getAllProducts";

	private double discountValue;
	private double iva;
	private double pvp;
	
	
	public double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(double discountValue) {
		this.discountValue = discountValue;
	}
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public double getPvp() {
		return pvp;
	}
	public void setPvp(double pvp) {
		this.pvp = pvp;
	}
	
    /** {@inheritDoc} */
    @Override
    protected Map<String, Object> getFields() {
        Map<String, Object> fieldsMap = new HashMap<>();

        fieldsMap.put("discountValue", discountValue);
        fieldsMap.put("iva", iva);
        fieldsMap.put("pvp", pvp);

        return fieldsMap;
    }
}
