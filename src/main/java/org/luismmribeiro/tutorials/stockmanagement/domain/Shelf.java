package org.luismmribeiro.tutorials.stockmanagement.domain;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The Shelf Entity
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Entity
@NamedQuery(name=Shelf.GET_ALL_SHELVES_QUERY_NAME, query="SELECT s FROM Shelf s")
@NamedQuery(name=Shelf.GET_SHELVES_BY_PRODUCT_ID_QUERY_NAME, query="SELECT s FROM Shelf s WHERE s.product.id = :productId")
public class Shelf extends GenericEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String GET_ALL_SHELVES_QUERY_NAME = "Shelf.getAllShelves";
	public static final String GET_SHELVES_BY_PRODUCT_ID_QUERY_NAME = "Shelf.getShelvesByProductId";

	@ManyToOne
	private Product product;
	private String capacity;
	private double rentPrice;

	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public Product getProduct() {
		return product;
	}	
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getRentPrice() {
		return rentPrice;
	}
	public void setRentPrice(double rentPrice) {
		this.rentPrice = rentPrice;
	}
	
    /** {@inheritDoc} */
    @Override
    protected Map<String, Object> getFields() {
        Map<String, Object> fieldsMap = new HashMap<>();

        fieldsMap.put("product", product);
        fieldsMap.put("capacity", capacity);
        fieldsMap.put("rentPrice", rentPrice);

        return fieldsMap;
    }
}
