package org.luismmribeiro.tutorials.stockmanagement.domain;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class for all the entities in this project
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@MappedSuperclass
public abstract class GenericEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public void setId(Long id) {
		this.id=id;
	}
	
	public Long getId() {
		return this.id;
	}
	
    /** {@inheritDoc} */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        setField(builder, "id", Objects.toString(getId(), ""));
        getFields().entrySet().stream().forEach(e -> {
            builder.append(",\n");
            setField(builder, e.getKey(), Objects.toString(e.getValue()));
        });
        return builder.append("]").toString();
    }

    private void setField(final StringBuilder builder, final String itemName, final String item) {
        builder.append("'").append(itemName).append("': '").append(item).append("'");
    }
    
    /**
     * This method exist for logging purposes.
     * It is used on toString method. It should return a map where the key is the name of the attribute and the value
     * is the correspondent value of the attribute. For each attribute of the entity there should be an entry in this
     * map. For each element of this map, the toString method will write a line where it will assign the name of the
     * attribute to its value.
     * 
     * @return A map where the key is the name of the attribute and the value is its value
     */
    protected Map<String, Object> getFields() {
        return Collections.emptyMap();
    }
}
