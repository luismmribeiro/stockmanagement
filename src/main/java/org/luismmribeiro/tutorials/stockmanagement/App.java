package org.luismmribeiro.tutorials.stockmanagement;

import javax.annotation.sql.DataSourceDefinition;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS configuration class
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@ApplicationPath("/stock")
public class App extends Application {
	/** DataSource to connect to database. */
    @DataSourceDefinition(
        name = "java:global/stockManagementDS",
        className = "org.h2.Driver",
        url = "jdbc:h2:mem:myDb;DB_CLOSE_DELAY=-1",
        user = "sa",
        password = "sa"
    )
    public static class IbbmDatasource {
        // Do nothing
    }
}

