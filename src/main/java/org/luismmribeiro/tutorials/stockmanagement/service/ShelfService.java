package org.luismmribeiro.tutorials.stockmanagement.service;

import java.util.List;

import org.luismmribeiro.tutorials.stockmanagement.domain.Shelf;
import org.luismmribeiro.tutorials.stockmanagement.repository.ShelfRepository;

/**
 * The Shelf service interface
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface ShelfService extends GenericService<ShelfRepository, Shelf> {
	/**
	 * Finds all the shelves that have the product with the given productId
	 * 
	 * @param id the id of the product
	 * @return A list of shelves that have the product with the given productId
	 */
	List<Shelf> findByProductId(long id);
}