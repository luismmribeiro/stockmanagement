package org.luismmribeiro.tutorials.stockmanagement.service;

import org.luismmribeiro.tutorials.stockmanagement.domain.Product;
import org.luismmribeiro.tutorials.stockmanagement.repository.ProductRepository;

/**
 * The Product service interface
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface ProductService extends GenericService<ProductRepository, Product> {
}