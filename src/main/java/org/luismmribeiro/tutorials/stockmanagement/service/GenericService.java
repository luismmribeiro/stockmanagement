package org.luismmribeiro.tutorials.stockmanagement.service;

import java.util.List;

import org.luismmribeiro.tutorials.stockmanagement.domain.GenericEntity;
import org.luismmribeiro.tutorials.stockmanagement.repository.GenericRepository;

/**
 * Generic interface to all the service interfaces
 * 
 * @param <R> The correspondent Repository of this service class
 * @param <E> The correspondent Entity of this service class
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface GenericService<R extends GenericRepository<E>, E extends GenericEntity> {
	/**
	 * Deletes the entity with the given id.
	 * 
	 * @param id The id of the entity to delete
	 */
	void delete(long id);

	/**
	 * Updates the entity in the persistent unit
	 * 
	 * @param id the id of the entity to persist
	 * @param entity the entity to persist
	 * @return The updated entity (the "attached" entity)
	 */
	E update(long id, E entity);

	/**
	 * Creates the entity in the persistent unit
	 * 
	 * @param entity the entity to persist
	 * @return The created entity (the "attached" entity)
	 */	
	E create(E entity);

	/**
	 * Gets all the entities of this type
	 * 
	 * @return All the entities of this type in the persistent unit
	 */
	List<E> getAll();

	/**
	 * Finds the entity of this type with the given id
	 * 
	 * @param id The id of the Entity to be retrieved
	 * @return The entity of this type with the given id
	 */
	E findById(long id);
}