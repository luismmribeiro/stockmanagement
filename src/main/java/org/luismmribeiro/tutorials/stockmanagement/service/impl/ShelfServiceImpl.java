package org.luismmribeiro.tutorials.stockmanagement.service.impl;

import java.util.List;

import org.luismmribeiro.tutorials.stockmanagement.domain.Shelf;
import org.luismmribeiro.tutorials.stockmanagement.repository.ShelfRepository;
import org.luismmribeiro.tutorials.stockmanagement.service.ShelfService;

/**
 * The Shelf service implementation class
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class ShelfServiceImpl extends AbstractService<ShelfRepository, Shelf> implements ShelfService {
	/** {@inheritDoc} */
	@Override
	public List<Shelf> findByProductId(long id) {
		return repository.findByProductId(id);
	}
}
