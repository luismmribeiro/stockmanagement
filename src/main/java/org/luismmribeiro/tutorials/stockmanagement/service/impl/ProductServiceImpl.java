package org.luismmribeiro.tutorials.stockmanagement.service.impl;

import org.luismmribeiro.tutorials.stockmanagement.domain.Product;
import org.luismmribeiro.tutorials.stockmanagement.repository.ProductRepository;
import org.luismmribeiro.tutorials.stockmanagement.service.ProductService;

/**
 * The Product service implementation class
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class ProductServiceImpl extends AbstractService<ProductRepository, Product> implements ProductService {
}
