package org.luismmribeiro.tutorials.stockmanagement.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import org.luismmribeiro.tutorials.stockmanagement.domain.GenericEntity;
import org.luismmribeiro.tutorials.stockmanagement.repository.GenericRepository;
import org.luismmribeiro.tutorials.stockmanagement.service.GenericService;

/**
 * The abstract class for all the service classes
 * 
 * @param <R> The repository class
 * @param <E> The entity class
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public abstract class AbstractService<R extends GenericRepository<E>, E extends GenericEntity> implements GenericService<R, E> {
	private static final Supplier<NotFoundException> NOT_FOUND_EXCEPTION_THROWER = 
			() -> new NotFoundException("The object with that id does not exist.");
	
	@Inject
	protected R repository;
	
	/** {@inheritDoc} */
	@Override
	@Transactional
	public void delete(long id) {
		E entity = repository.findById(id).orElseThrow(NOT_FOUND_EXCEPTION_THROWER);
		repository.delete(entity);
	}

	/** {@inheritDoc} */
	@Override
	@Transactional
	public E update(long id, E entity) {
		if(Objects.isNull(entity)) {
			throw new BadRequestException("The entity should not be null");
		}
		
		repository.findById(id).orElseThrow(NOT_FOUND_EXCEPTION_THROWER);
		entity.setId(id);
		
		return repository.update(entity);
	}
	
	/** {@inheritDoc} */
	@Override
	@Transactional
	public E create(E entity) {
		if(Objects.isNull(entity)) {
			throw new BadRequestException("The entity should not be null");
		}
		if(Objects.nonNull(entity.getId())) {
			throw new BadRequestException("The provided entity should not have an id");
		}
		return repository.create(entity);
	}
	
	/** {@inheritDoc} */
	@Override
	public List<E> getAll() {
		return repository.getAll();
	}
	
	/** {@inheritDoc} */
	@Override
	public E findById(long id) {
		return repository
				.findById(id)
				.orElseThrow(NOT_FOUND_EXCEPTION_THROWER);
	}
}
