package org.luismmribeiro.tutorials.stockmanagement.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.luismmribeiro.tutorials.shared.logging.Logged;
import org.luismmribeiro.tutorials.stockmanagement.domain.Product;
import org.luismmribeiro.tutorials.stockmanagement.repository.ProductRepository;
import org.luismmribeiro.tutorials.stockmanagement.service.ProductService;

/**
 * The Product controller class. It is the API entry for Products.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Path("products")
public class ProductController extends AbstractController<ProductService, ProductRepository, Product> {
	/**
	 * Gets all the system Products
	 * 
	 * @return All the products
	 */
	@Logged
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAll() {
		return business.getAll();
	}
}
