package org.luismmribeiro.tutorials.stockmanagement.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.luismmribeiro.tutorials.shared.logging.Logged;
import org.luismmribeiro.tutorials.stockmanagement.domain.Shelf;
import org.luismmribeiro.tutorials.stockmanagement.repository.ShelfRepository;
import org.luismmribeiro.tutorials.stockmanagement.service.ShelfService;

/**
 * The Shelf controller class. It is the API entry for Shelves.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Path("shelves")
public class ShelfController extends AbstractController<ShelfService, ShelfRepository, Shelf> {
	/**
	 * Gets the system Shelves. Either all or the ones that have the product with the given productId.
	 * 
	 * @param id optional parameter stating the productId of the shelves
	 * @return If the provided productId is null, it returns all the Shelves. Otherwise, it returns the
	 * the shelves that have the product of the given productId
	 */
	@Logged
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Shelf> getShelves(@QueryParam("productId") Long id) {
		if(id == null) {
			return business.getAll();
		}
		else {
			return business.findByProductId(id);
		}
	}
}
