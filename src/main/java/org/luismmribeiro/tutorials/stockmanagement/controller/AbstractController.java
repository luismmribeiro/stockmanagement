package org.luismmribeiro.tutorials.stockmanagement.controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.luismmribeiro.tutorials.shared.logging.Logged;
import org.luismmribeiro.tutorials.stockmanagement.domain.GenericEntity;
import org.luismmribeiro.tutorials.stockmanagement.repository.GenericRepository;
import org.luismmribeiro.tutorials.stockmanagement.service.GenericService;

/**
 * The abstract controller class for all the JAX-RS resources in this project
 * 
 * @param <B> The business class
 * @param <R> The repository class
 * @param <E> The Entity class
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public abstract class AbstractController<B extends GenericService<R, E>, R extends GenericRepository<E> , E extends GenericEntity> {
	@Inject
	protected B business;
	@Context
	protected UriInfo context;


	/**
	 * This is not a correct REST URI, but it is here just to debug purposes
	 * 
	 * @return String with the health check status
	 */
	@Logged
	@GET
	@Path("healthCheck")
	@Produces(MediaType.TEXT_PLAIN)
	public String healthCheck() {
		return "URI " + context.getRequestUri().toString() + " is OK!";
	}

	/**
	 * Creates an entity in the system
	 * 
	 * @param entity The entity to create
	 * @return The created entity
	 */
	@Logged
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public E create(E entity) {
		return business.create(entity);
	}

	/**
	 * Retrieves an entity given its id
	 * 
	 * @param id The id of the entity
	 * @return the entity with the given id
	 */
	@Logged
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public E findById(@PathParam("id") long id) {
		return business.findById(id);
	}

	/**
	 * Updates an entity in the system
	 * 
	 * @param id The id of the entity
	 * @return the updated entity
	 */
	@Logged
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public E update(@PathParam("id") long id, E entity) {
		return business.update(id, entity);
	}

	/**
	 * Removes an entity from the system
	 * 
	 * @param id The id of the entity to remove
	 * @return A success code
	 */
	@Logged
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") long id) {
		business.delete(id);
		return Response.ok().build();
	}
}