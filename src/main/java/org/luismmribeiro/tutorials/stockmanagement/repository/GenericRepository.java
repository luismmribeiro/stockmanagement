package org.luismmribeiro.tutorials.stockmanagement.repository;

import java.util.List;
import java.util.Optional;

import org.luismmribeiro.tutorials.stockmanagement.domain.GenericEntity;

/**
 * The generic repository interface
 * 
 * @param <E> The entity class
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface GenericRepository<E extends GenericEntity> {
	/**
	 * Deletes the given entity
	 * 
	 * @param entity The entity to delete
	 */
	void delete(E entity);

	/**
	 * Updates the entity in the persistent unit
	 * 
	 * @param entity the entity to persist
	 * @return The updated entity (the "attached" entity)
	 */
	E update(E entity);

	/**
	 * Creates the entity in the persistent unit
	 * 
	 * @param entity the entity to persist
	 * @return The created entity (the "attached" entity)
	 */
	E create(E entity);

	/**
	 * Gets all the entities of this type
	 * 
	 * @return All the entities of this type in the persistent unit
	 */
	List<E> getAll();

	/**
	 * Finds the entity of this type with the given id
	 * 
	 * @param id The id of the Entity to be retrieved
	 * @return An Optional object containing the entity of this type with the given id (if it exists)
	 */
	Optional<E> findById(long id);
}