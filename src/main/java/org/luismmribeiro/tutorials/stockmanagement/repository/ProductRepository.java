package org.luismmribeiro.tutorials.stockmanagement.repository;

import org.luismmribeiro.tutorials.stockmanagement.domain.Product;

/**
 * The Product repository
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface ProductRepository extends GenericRepository<Product> {

}