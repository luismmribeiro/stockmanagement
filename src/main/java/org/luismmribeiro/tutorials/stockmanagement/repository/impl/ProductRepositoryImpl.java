package org.luismmribeiro.tutorials.stockmanagement.repository.impl;

import org.luismmribeiro.tutorials.stockmanagement.domain.Product;
import org.luismmribeiro.tutorials.stockmanagement.repository.ProductRepository;

/**
 * The implementation of the Product repository
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class ProductRepositoryImpl extends AbstractRepository<Product> implements ProductRepository {
	/** {@inheritDoc} */
	@Override
	protected Class<Product> getEntityClass() {
		return Product.class;
	}
	
	/** {@inheritDoc} */
	@Override
	protected String getAllEntityQueryName() {
		return Product.GET_ALL_PRODUCTS_QUERY_NAME;
	}
}
