package org.luismmribeiro.tutorials.stockmanagement.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.luismmribeiro.tutorials.stockmanagement.domain.GenericEntity;
import org.luismmribeiro.tutorials.stockmanagement.repository.GenericRepository;

/**
 * The abstract class for all the Repositories
 * 
 * @param <E> The entity class
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public abstract class AbstractRepository<E extends GenericEntity> implements GenericRepository<E> {
	@PersistenceContext
	protected EntityManager entityManager;
	
	/** {@inheritDoc} */
	@Override
	public void delete(E entity) {
		entityManager.remove(entity);
	}

	/** {@inheritDoc} */
	@Override
	public E update(E entity) {
		return entityManager.merge(entity);
	}
	
	/** {@inheritDoc} */
	@Override
	public E create(E entity) {
		return entityManager.merge(entity);
	}
	
	/** {@inheritDoc} */
	@Override
	public List<E> getAll() {
		return entityManager.createNamedQuery(getAllEntityQueryName(), getEntityClass()).getResultList();
	}
	
	/** {@inheritDoc} */
	@Override
	public Optional<E> findById(long id) {
		return Optional.ofNullable(entityManager.find(getEntityClass(), id));
	}

	/**
	 * Helper method that returns the class of this Repository's entity
	 * 
	 * @return This repository's entity class
	 */
	protected abstract Class<E> getEntityClass();
	
	/**
	 * Helper method that returns the name of the named query that returns all the entities of this repository
	 * 
	 * @return the named query name
	 */
	protected abstract String getAllEntityQueryName();
}
