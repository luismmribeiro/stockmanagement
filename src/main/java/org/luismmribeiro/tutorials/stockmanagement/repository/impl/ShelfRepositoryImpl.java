package org.luismmribeiro.tutorials.stockmanagement.repository.impl;

import java.util.List;

import org.luismmribeiro.tutorials.stockmanagement.domain.Shelf;
import org.luismmribeiro.tutorials.stockmanagement.repository.ShelfRepository;

/**
 * The implementation of the Shelf repository
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class ShelfRepositoryImpl extends AbstractRepository<Shelf> implements ShelfRepository {
	/** {@inheritDoc} */
	@Override
	protected Class<Shelf> getEntityClass() {
		return Shelf.class;
	}

	/** {@inheritDoc} */
	@Override
	protected String getAllEntityQueryName() {
		return Shelf.GET_ALL_SHELVES_QUERY_NAME;
	}
	
	/** {@inheritDoc} */
	@Override
	public List<Shelf> findByProductId(long id) {
		return entityManager
				.createNamedQuery(Shelf.GET_SHELVES_BY_PRODUCT_ID_QUERY_NAME, Shelf.class)
				.setParameter("productId", id)
				.getResultList();
	}
}
