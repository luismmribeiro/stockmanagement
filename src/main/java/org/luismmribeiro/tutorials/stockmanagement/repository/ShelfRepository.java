package org.luismmribeiro.tutorials.stockmanagement.repository;

import java.util.List;

import org.luismmribeiro.tutorials.stockmanagement.domain.Shelf;

/**
 * The Shelf repository
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface ShelfRepository extends GenericRepository<Shelf> {
	/**
	 * Finds all the shelves that have the product with the given productId
	 * 
	 * @param id the id of the product
	 * @return A list of shelves that have the product with the given productId
	 */
	List<Shelf> findByProductId(long id);
}