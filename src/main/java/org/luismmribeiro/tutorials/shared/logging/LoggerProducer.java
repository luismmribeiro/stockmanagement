package org.luismmribeiro.tutorials.shared.logging;

import java.util.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;


/**
 * CDI logger producer class.
 *
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class LoggerProducer {
    /**
     * Method that produces the Logger class as a CDI bean.
     * 
     * @param injectionPoint the InjectionPoint
     * @return The logger
     */
    @Produces
    public Logger produceLogger(final InjectionPoint injectionPoint) {
    	return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }  
}
