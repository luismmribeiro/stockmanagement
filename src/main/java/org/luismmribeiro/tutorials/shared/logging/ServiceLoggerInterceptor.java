package org.luismmribeiro.tutorials.shared.logging;

import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * Intercepter class that logs the calls to CDI bean's methods. It logs the class name, the method name, the parameters
 * and also the returned object.
 * 
 * @author lmmribeiro
 */
@Logged
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
public class ServiceLoggerInterceptor {
    @Inject
    private Logger logger;

    /**
     * Intercepter method for every CDI bean method annotated with @Logged annotation.
     * 
     * @param invocationContext The invocationContext
     * @return The original method return object
     * @throws Exception In case of exception on the invoked method.
     */
    @AroundInvoke
    public Object logMethodEntry(final InvocationContext invocationContext) throws Exception {
        long initTime = System.currentTimeMillis();
        String methodName = invocationContext.getTarget().getClass().getName() + "." 
                + invocationContext.getMethod().getName();
        logger.info("Entering method: " + methodName);
        Object[] parameterObjects = invocationContext.getParameters();
        for (int i = 0; i < parameterObjects.length; i++) {
            logger.info("Parameter " + i + " : " + parameterObjects[i]);
        }
        
        Object obj = invocationContext.proceed();
        
        logger.info("Returned object: " + obj);
        logger.info("Leaving method " + methodName + ". It took: " + (System.currentTimeMillis() - initTime) + "ms");
        
        return obj;
    }
}
