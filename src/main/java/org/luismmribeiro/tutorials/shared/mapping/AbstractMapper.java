package org.luismmribeiro.tutorials.shared.mapping;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.eclipse.microprofile.config.inject.ConfigProperty;


/**
 * An abstract mapper that performs most of the logic of our mappers
 * 
 * @param <E> The exception to be mapped
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public abstract class AbstractMapper<E extends Throwable> implements ExceptionMapper<E> {
	@Inject
    @ConfigProperty(name = "mapping.response_type")
    private String responseType;
	
	@Inject
	private Logger logger;

    /** {@inheritDoc} */
    @Override
    public Response toResponse(E exception) {
        handleExceptionLogging(exception);

        Status status = getStatus(exception);
        ErrorDto dto = ErrorDto.getBuilder()
                .status(status.getStatusCode())
                .title(status.getReasonPhrase())
                .detail(getMessage(exception))
                .build();
        return Response.status(status)
                .type(responseType)
                .entity(dto)
                .build();
    }

    /**
     * Returns the message to send to the client.
     * 
     * @param exception the thrown exception
     * @return the message to send to the client
     */
    public String getMessage(E exception) {
        return exception.getMessage();
    }

    /**
     * Returns the status to send to the client.
     * 
     * @param exception the exception
     * @return the status to send to the client
     */
    public Status getStatus(E exception) {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }

    /**
     * Defines if the exception must be logged or not. It should be replaced by a logger.
     * 
     * @param exception the trapped exception
     */
    public void handleExceptionLogging(E exception) {
    	logger.log(Level.SEVERE, exception.getMessage(), exception);
    }
}
