package org.luismmribeiro.tutorials.shared.mapping;

import javax.transaction.TransactionalException;
import javax.ws.rs.ext.Provider;

/**
 * A mapper that takes care of the TransactionalException and generates a corresponding response message.
 * This Mapper has the same functionality of the ThrowableMapper. However, we were forced to implement
 * a new mapper because JAX-RS implements a TransactionalException mapper with the default behaviour
 * which means that it generates an HTML page with the error description. To overcome this problem we
 * create this Mapper specific for TransactionalExceptions.
 *
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Provider
public class TransactionalMapper extends AbstractMapper<TransactionalException> {
    /** {@inheritDoc} */
    @Override
    public String getMessage(TransactionalException exception) {
        return "The application backend returned an unexpected error. "
                + "Please check the logs to understand its cause.";
    }
}
