package org.luismmribeiro.tutorials.shared.mapping;

import java.io.Serializable;

/**
 * Error object. It will be sent when there is an error on the application.
 *
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class ErrorDto implements Serializable {
    /** serialVersionUID */
    private static final long serialVersionUID = 1L;
    
    private int status;
    private String title;
    private String detail;
    

    /**
     * The builder class.
     *
     * @author Luis Ribeiro <luismmribeiro@gmail.com>
     */
    public static class Builder {
        private ErrorDto thisDto;

        /**
         * Constructor.
         */
        public Builder() {
            this.thisDto = new ErrorDto();
        }

        /**
         * Builds.
         *
         * @return Current builder
         */
        public ErrorDto build() {
            return this.thisDto;
        }

        /**
         * Sets status parameter.
         *
         * @param status The status
         * @return Current builder
         */
        public Builder status(int status) {
            this.thisDto.status = status;
            return this;
        }

        /**
         * Sets detail parameter.
         *
         * @param detail The detail
         * @return Current builder
         */
        public Builder detail(String detail) {
            this.thisDto.detail = detail;
            return this;
        }

        /**
         * Sets title parameter.
         *
         * @param title The title
         * @return Current builder
         */
        public Builder title(String title) {
            this.thisDto.title = title;
            return this;
        }
    }
    
    public static Builder getBuilder() {
        return new Builder();
    }
    
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDetail() {
        return detail;
    }
    public void setDetail(String detail) {
        this.detail = detail;
    }
}
