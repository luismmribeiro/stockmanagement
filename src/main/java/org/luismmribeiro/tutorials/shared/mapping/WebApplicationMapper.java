package org.luismmribeiro.tutorials.shared.mapping;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

/**
 * A mapper that generates a controlled message when a WebApplicationException occurs.
 *
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Provider
public class WebApplicationMapper extends AbstractMapper<WebApplicationException> {
    /** {@inheritDoc} */
    @Override
    public Status getStatus(WebApplicationException exception) {
        return Status.fromStatusCode(exception.getResponse().getStatus());
    }
}
