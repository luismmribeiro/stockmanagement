package org.luismmribeiro.tutorials.shared.mapping;

import javax.ws.rs.ext.Provider;

/**
 * A generic mapper that transforms all the Throwables into a JSON internal server error response.
 *
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@Provider
public class ThrowableMapper extends AbstractMapper<Throwable> {
    /** {@inheritDoc} */
    @Override
    public String getMessage(Throwable exception) {
        return "The application backend returned an unexpected error. "
                + "Please check the logs to understand its cause.";
    }
}
