# stockManagement
This project was created to provide a small example of a JAX-RS project with CDI and JPA, running on micro Payara 5.193.

The code was developed by Luis Ribeiro and it provides no warranties what so ever.

In order to correctly and automatically use this code you should have Apache Maven 3.5.0+ installed on your workstation. If that is the case, you can _start_ this application using the following command on your project root:
```bash
  mvn clean package payara-micro:start
```